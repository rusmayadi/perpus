@extends('layouts.master-admin-layout')

@section('title')
Tambah Buku
@stop

@section('content')
<div class="card p-4">
	<form action="{{route('admin.prosestambahbuku')}}" method="post">
		@csrf
		<div class="form-group">
			<label for="exampleInputEmail1">Nama Buku</label>
			<input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Penerbit</label>
			<input type="text" name="penerbit" class="form-control" id="exampleInputPassword1">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Penulis</label>
			<input type="text" name="penulis" class="form-control" id="exampleInputPassword1">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Tahun Terbit</label>
			<input type="text" name="tahun_penerbit" class="form-control" id="exampleInputPassword1">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Tempat Terit</label>
			<input type="text" name="tempat_terbit" class="form-control" id="exampleInputPassword1">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">Edisi</label>
			<input type="text" name="edisi" class="form-control" id="exampleInputPassword1">
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
</div>
@stop