@extends('layouts.master-admin-layout')

@section('css')
<style type="text/css">
	.circle {
		height: 13px;
		width: 13px;
		border-radius: 50%;
		display: inline-block;
	}
</style>
@stop

@section('content')
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800">Tables</h1>
	<p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<div class="row">
				<div class="col"><h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6></div>
				<div class="col">
					<a href="{{route('admin.tambahBuku')}}" class="btn btn-success btn-circle float-right">
						<i class="fas fa-plus"></i>
					</a>
				</div>
			</div>
			
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Buku</th>
							<th>Penerbit</th>
							<th>Penulis</th>
							<th>Tahun Terbit</th>
							<th>Edisi</th>
							<th>No Buku</th>
							<th>Status Buku</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>No</th>
							<th>Nama Buku</th>
							<th>Penerbit</th>
							<th>Penulis</th>
							<th>Tahun Terbit</th>
							<th>Edisi</th>
							<th>No Buku</th>
							<th>Status Buku</th>
							<th>Aksi</th>		
						</tr>
					</tfoot>
					@php 
					$no=1;
					@endphp
					<tbody>
						@foreach($buku as $b)
						<tr>
							<td>{{$no++}}</td>
							<td>{{ $b->nama }}</td>
							<td>{{	$b->penerbit }}</td>
							<td>{{ $b->penulis }}</td>
							<td>{{ $b->tahun_penerbit }}</td>
							<td>{{ $b->edisi }}</td>
							<td>{{ $b->no_buku }}</td>
							<td>
								@if($b->status_buku == 'tersedia')
								<span class="circle bg-success"></span> Tersedia
								@elseif($b->status_buku == 'dipinjam')
								<span class="circle bg-warning"></span> Dipinjam
								@endif
							</td>
							<td>
								<a href="{{ route('admin.hapusBuku',$b->buku_id)}}" class="btn btn-sm btn-primary">Hapus</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
@stop

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('#dataTable').DataTable();
	});
</script>
@stop