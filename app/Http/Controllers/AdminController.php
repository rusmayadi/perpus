<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Buku;

class AdminController extends Controller
{
    public function home()
    {
    	return view('admin.home');
    }
    public function dataBuku()
    {
    	$buku=DB::table('buku')
    	->where('status_buku', 'tersedia')
    	->orWhere('status_buku', 'dipinjam')
    	->get();

    	return view('admin.data-master.buku.data', compact('buku'));
    }

    public function hapusBuku($id){
    	$buku=Buku::find($id);
    	$buku->status_buku='terhapus';
    	$buku->save();
    	return redirect()->back();
    }

    public function tambahBuku(){
    	return view('admin.data-master.buku.tambah');

    }

    public function prosestambahbuku(Request $req){
    	$postbuku=new Buku;
    	$postbuku->nama=$req->nama;
    	$postbuku->penerbit=$req->penerbit;
    	$postbuku->penulis=$req->penulis;
    	$postbuku->tahun_PENERBIT=$req->tahun_penerbit;
    	$postbuku->tempat_terbit=$req->tempat_terbit;
    	$postbuku->edisi=$req->edisi;
    	$kode_buku=rand();
    	$postbuku->no_buku=$kode_buku;
    	$postbuku->status_buku='tersedia';
    	$postbuku->save();
    	return redirect()->back();
    }
}
