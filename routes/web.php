<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('admin')->group(function(){
	Route::get('/', 'AdminController@home')
	->name('admin.home');
	Route::get('/data-buku', 'AdminController@dataBuku')
	->name('admin.data_buku');

	Route::get('/hapus_buku/{id}', 'AdminController@hapusBuku')
	->name('admin.hapusBuku');

	Route::get('/tambah', 'AdminController@tambahBuku')
	->name('admin.tambahBuku');

	Route::post('/prosestambahbuku', 'AdminController@prosestambahbuku')
	->name('admin.prosestambahbuku');

});

// 
