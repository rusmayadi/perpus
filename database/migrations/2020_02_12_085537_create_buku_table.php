<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->bigIncrements('buku_id');
            $table->string('nama');
            $table->string('penerbit');
            $table->string('penulis');
            $table->string('tahun_penerbit');
            $table->string('tempat_terbit');
            $table->string('edisi');
            $table->string('no_buku')->unique();
            $table->string('status_buku')->default('tersedia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
